import java.util.ArrayList;
import java.util.*;
import java.io.*;
import java.lang.Comparable;
public class Heapsort{
	
	public static ArrayList<String> stringArray = new ArrayList<String>();
	
	private static <AnyType extends Comparable<? super AnyType>> void percDown(ArrayList<AnyType> a, int i, int n ) { 
		int child; 
		AnyType tmp; 
		for(tmp = a.get(i); (2*i+1)< n; i =child ){ 
			child = 2*i+1; 
			if(child != n - 1 && a.get(child).compareTo(a.get(child + 1))<0) 
				child++; 
			if( tmp.compareTo( a.get(child))<0) 
				a.set(i,a.get(child));
			 else 
				break; 
		} 
		a.set(i,tmp); 
	}
	
	 public static <AnyType extends Comparable<? super AnyType>>  void heapSort(ArrayList<AnyType> a){
		for( int i = a.size() / 2 - 1; i >= 0; i-- ) /* buildHeap */ 
			percDown( a, i, a.size() ); 
		for( int i = a.size() - 1; i > 0; i-- ){ 
			int index1=0;
			int index2=i;
			AnyType tmp = a.get(index1);
			a.set(index1,a.get(index2));
			a.set(index2,tmp);
			
			percDown( a, 0, i );  
		} 
	}
	public static ArrayList<String> heapSortRunner(String fileName){

		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
			String line;
			while ((line = br.readLine()) != null) {
			   stringArray.add(line);
			}
		}
		catch (Exception e){
			System.err.println(e.getMessage());
		}
		heapSort(stringArray);
		return stringArray;
	}
	public static void main(String[] args){
		ArrayList<String> d;
		d=heapSortRunner("test.txt");
		System.out.println(Arrays.toString(stringArray.toArray()));
	}
}