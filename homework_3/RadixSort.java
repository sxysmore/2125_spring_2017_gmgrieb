import java.util.ArrayList; 
import java.util.List; 
import java.util.Arrays;
public class RadixSort{
	 public static void radixSortA( ArrayList<String> a, int stringLen ){ 
		final int BUCKET = 256;
		ArrayList<String> buckets[] = new ArrayList[ BUCKET ]; 
		for( int i = 0; i < BUCKET; i++ )
			buckets[ i ] = new ArrayList<>( ); 
		for( int pos = stringLen - 1; pos >= 0; pos-- ){ 
			for( String s : a )
				buckets[ s.charAt( pos ) ].add( s ); 
			int idx = 0;
			for( ArrayList<String> thisBucket : buckets ){
				for( String s : thisBucket )
					a.set(idx++,s); 
				thisBucket.clear( ); 
			}
		}
	}
	public static void main(String[] args){
		ArrayList<String> t=new ArrayList<String>();
		t.add("c");
		t.add("a");
		t.add("b");
		radixSortA(t,1);
		System.out.println(Arrays.toString(t.toArray()));
	}
}