import java.util.NoSuchElementException;
/**
*A basic implementation of a Queue buit on
*a circular array.
*@author Graham Grieb
*@version 17 October 2016
*/
public class Queue<T>{
	private int head; //the index of the first element to remove
	private int tail; //the first available index to enqueue
	private int size; //the length of the array
	private T[] array;
	/**
	*Constructor intitialize array to size of 5.
	*/
	@SuppressWarnings("unchecked")
	public Queue(){
		this.size=5;
		array=(T[])new Object[this.size];
		this.head=0;
		this.tail=0;
	}
	
	/**
	*Adds an element  at the end of the queue
	*@param 	element the ekement to add
	*/
	public void enqueue(T element){
		if(this.isFull()){
			this.resizeArray();
		}
		this.array[tail]=element;
		this.tail=(this.tail+1)%this.size;
		
	}
	/**
	*Returns whether or not the underlying array has
	*any empty spaces.
	*@return whether this is full
	*/
	private boolean isFull(){
		if(((this.tail+1)%this.size)==this.head){
			return true;
		}
		else{
			return false;
		}
	}
	/**
	*Resizes the underlying array to double its previous capacity.
	*/
	@SuppressWarnings("unchecked")
	private void resizeArray(){
		T[] temp = (T[])new Object[this.size*2];
		int elementsCopied=0;
		for(int i = this.head;i!=this.tail;i=(i+1)%this.size){
			temp[elementsCopied]= this.array[i];
			elementsCopied++;
		}
		this.size*=2;
		this.head=0;
		this.tail=elementsCopied;
		this.array=temp;
	}
	/**
	*Removes an element  at the end of the queue
	*/
	public T dequeue(){
		if(this.isEmpty()){
			throw new NoSuchElementException("Queue is empty.");
		}
		else{
			T temp= this.array[this.head];
			this.head=(this.head+1)%this.size;
			return temp;
			
		}
	}
    /**
     *returns wheter the queue is empty
     * 
     * @return boolean	true if Queue is empty
     */
	
	private boolean isEmpty(){
		if(this.head==this.tail){
			return true;
		}
		else{
			return false;
		}
	}
	
}