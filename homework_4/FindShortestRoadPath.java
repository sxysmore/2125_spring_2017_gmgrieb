import java.util.ArrayList;
import java.util.*;
import java.io.*;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
public class FindShortestRoadPath{
		/*Gr:Each line that contains an edge starts with the
	character 'a'. It is followed by the id of the starting node, then the
	id of the ending node, finally the weight of that edge.
	
	The .co file is the coordinates file. Every vertex line begins with the
	character 'v'. It is followed by the node id and then the longitude and
	latitude 
	*/
	public static String allPrevious(Vertex a){
		if(a.prev==-1){
			return "";
		}
		else{
			return ""+a.prev+","+allPrevious(vertices.get(a.prev));
		}
	}
	public static int sourceID;
	public static int targetID;
	public static Map<Integer,Vertex> vertices=new HashMap<Integer,Vertex>();
	public static void main(String[] args) throws FileNotFoundException{
		sourceID=Integer.parseInt(args[1]);
		targetID=Integer.parseInt(args[2]);
		
		String line = "";
		File f = new File(args[0]);
		Scanner fileReader = new Scanner(f);
		//vertices.add(new Vertex(0));
		while(fileReader.hasNextLine()){
            // read in the line and split it
            line = fileReader.nextLine();
            String[] tokens = line.split(" ");
			
            
            if(tokens[0].equals("a")){ // if the line lists a vertex
                int firstNode = Integer.parseInt(tokens[1]);
				//System.out.println(firstNode);
				Vertex tmp=new Vertex(firstNode);
				if(!(vertices.containsKey(firstNode))){
					vertices.put(firstNode,tmp);
				}
				
				
				
				
				vertices.get(firstNode).adj.put(Integer.parseInt(tokens[2]),(Integer.parseInt(tokens[3])));
				//vertices.get(firstNode).adj.add(Integer.parseInt(tokens[2]));
				//vertices.get(firstNode).adjWeight.add(Integer.parseInt(tokens[3]));
				
				
				
				
				
                
            }
        }
		System.out.println(vertices.keySet());
		
		
		
	int currentVertex=sourceID;	
	vertices.get(currentVertex).dist=0;
	ArrayList<Vertex> knownDistances=new ArrayList<Vertex>();
	while(currentVertex!=targetID){
		System.out.println("kys");
		System.out.println(currentVertex);
		Integer[] adjArray=new Integer[vertices.get(currentVertex).adj.size()];
		adjArray=vertices.get(currentVertex).adj.keySet().toArray(adjArray);//array of adj 
		
		
		for(int b=0;b<adjArray.length;b++){//moves through adjacent list
			
			if(vertices.get(adjArray[b]).dist==0&&vertices.get(adjArray[b]).known==false){//if node is new
				vertices.get(adjArray[b]).dist=vertices.get(currentVertex).adj.get(adjArray[b])+vertices.get(currentVertex).dist;
				vertices.get(adjArray[b]).prev=vertices.get(currentVertex).id;
			}
			else if(vertices.get(currentVertex).adj.get(adjArray[b])<vertices.get(adjArray[b]).dist&vertices.get(adjArray[b]).known==false){//if its old distance is less than new distance
				vertices.get(adjArray[b]).dist=vertices.get(currentVertex).adj.get(adjArray[b])+vertices.get(currentVertex).dist;
				vertices.get(adjArray[b]).prev=vertices.get(currentVertex).id;			
			}
			if(vertices.get(adjArray[b]).known==false&&vertices.get(adjArray[b]).used==false){
				knownDistances.add(vertices.get(adjArray[b]));
				vertices.get(adjArray[b]).used=true;
				System.out.println("\t"+b+" "+adjArray[b]);
			}
			
		}
		
		vertices.get(currentVertex).known=true;
		int smallestVertex=-1;
		for(int i=0;i<knownDistances.size();i++){
			
			if(knownDistances.get(i).adj.size()==0){
				knownDistances.get(i).known=true;
			}
			if(smallestVertex==-1&&knownDistances.get(i).known==false){
				smallestVertex=i;	
			}
			else if(knownDistances.get(i).dist<knownDistances.get(smallestVertex).dist&&knownDistances.get(i).known==false)
				smallestVertex=i;
			System.out.println("\t"+"\t"+knownDistances.get(i).id+" "+knownDistances.get(i).dist+" "+smallestVertex);
		}
		currentVertex=knownDistances.get(smallestVertex).id;
		knownDistances.remove(smallestVertex);
			
	}
	/*for(int i=0;i<verticesArray.length;i++){
		Vertex a=vertices.get(verticesArray[i]);//Vertex we are at
		Integer[] adjArray=new Integer[a.adj.size()];
		adjArray=a.adj.keySet().toArray(adjArray);//array of adj
		for(int b=0;b<adjArray.length;b++){//moves through adjacent list
			if(a.adj.get(adjArray[b])<vertices.get(adjArray[b]).dist){
				vertices.get(adjArray[b]).dist=adjArray[b];			
			}
		}
	}*/
	//vertices.get(2).dist=(vertices.get(currentVertex).adj.get(adjArray[0]));
	//System.out.println(vertices.get(2).dist);
	System.out.println("Final Vertex: "+currentVertex+"\nDistance from Original: "+vertices.get(currentVertex).dist+"\nPrevious Nodes: "+allPrevious(vertices.get(currentVertex)));
		/*for each Vertex v { 
			v.dist = INFINITY; 
			v.known = false; 
		}
		s.dist = 0;
	while( there is an unknown distance vertex ){
		Vertex v = smallest unknown distance vertex;
		v.known = true;
		for each Vertex w adjacent to v if( !w.known ) { 
			DistType cvw = cost of edge from v to w;
		if( v.dist + cvw < w.dist ) { 
		// Update w 
		decrease( w.dist to v.dist + cvw ); 
		w.path = v; }
}*/
}
	}
